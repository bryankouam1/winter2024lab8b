public enum Tile{
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	private final String name;
	
	//constructor for String name
	private Tile(String name){
		this.name = name;
	}
	//getter for name field
	public String getName(){
		return this.name;
	}
}