import java.util.Scanner;
public class BoardGameApp{
	public static void main (String[]args){
		Scanner reader = new Scanner(System.in);
		
		Board game = new Board();
		System.out.println("Welcome User!!!");
		int numCastles= 7;
		int turns = 0;
		while(numCastles> 0 && turns<8){
			System.out.println("There is "+numCastles +" and "+ turns+ " turns.");
			System.out.println(game);
			
			System.out.println("Enter a row:");
			int row= Integer.parseInt(reader.nextLine());
		
			System.out.println("Enter a column: ");
			int col= Integer.parseInt(reader.nextLine());
			
			int gameToken=game.placeToken(row,col);
			while(gameToken <0){
				System.out.println("Enter a new row:");
				row= Integer.parseInt(reader.nextLine());
				
				System.out.println("Enter a new column ");
				col= Integer.parseInt(reader.nextLine());
				
				gameToken=game.placeToken(row,col);
			}
			if(gameToken == 1){
				System.out.println("WARNING!!!There is wall at that position!");
			}
			else{
				System.out.println("Castle Tile was succesfully placed!");
				numCastles--;
			}
			turns++;
		}

		
		System.out.println(game);
		
		if(numCastles==0){
			System.out.println("You won YAYY!!");
		}
		else{
			System.out.println("You lost WOMP WOMP ;(");
		}
		
		
	}
}