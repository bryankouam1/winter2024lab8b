import java.util.Random;
public class Board{
	//fields
	private Tile[][] grid;
	private final int SIZE=5;
	//constructor
	public Board() {
		Random rand = new Random();
		int indexOfArray = rand.nextInt(SIZE);
        this.grid = new Tile[SIZE][SIZE]; // Initialize the grid array
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                grid[i][j] = Tile.BLANK;	// Set each cell to BLANK
            }
			grid[i][indexOfArray] = Tile.HIDDEN_WALL;
        }
    }
	//toString
	public String toString(){
		String representationOFBoard="";
		for(int i=0;i<grid.length;i++){
			for(int j=0;j<grid[i].length;j++){
				
				representationOFBoard += "|"+this.grid[i][j].getName();
			}
			representationOFBoard+="|\n";
		}
		return representationOFBoard;
	}
	//constructors
	public int getSize(){
		return SIZE;
	}
	public Tile[][] getGrid(){
		return this.grid;
	}
	//placeToken method
	public int placeToken(int row, int col) {
    // Check if row and column values are in the correct range for a 5x5 board
		if (!(row >= 0 && row < grid.length && col >= 0 && col < grid[0].length)) {
			return -2;
		}

    // Check if the grid at the specified row and column is (BLANK)
		if (grid[row][col] == Tile.CASTLE && grid[row][col] == Tile.WALL) {
			
			return -1; 
		}
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	

	


}